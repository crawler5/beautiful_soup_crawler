import re
from urllib.request import urlopen, Request

from bs4 import BeautifulSoup

html = urlopen("http://www.jalanow.com/")
bs = BeautifulSoup(html, "html.parser")
images = bs.find_all("img", {"src":re.compile(".jpg")})
links = []
for image in images:
    link = image.get("src")
    links.append(link)

user_agent = USER_AGENT = 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_10_5) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/55.0.2883.95 Safari/537.36'
headers = {'User-Agent': user_agent }
for i, link in enumerate(links):
    filename = "img{}.jpg".format(i)
    r = Request(url=link, headers=headers)
    html = urlopen(r).read()
    with open(filename, 'wb') as f:
        f.write(r.content)
